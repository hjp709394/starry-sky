/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.starrysky;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int split_line_color=0x7f040000;
    }
    public static final class drawable {
        public static final int bk=0x7f020000;
        public static final int dot32=0x7f020001;
        public static final int dot64=0x7f020002;
        public static final int dot64_=0x7f020003;
        public static final int dot8=0x7f020004;
        public static final int ic_launcher=0x7f020005;
    }
    public static final class id {
        public static final int aboutButton=0x7f07000f;
        public static final int cameraView=0x7f070008;
        public static final int helpButton=0x7f07000e;
        public static final int latitudeEditText=0x7f070004;
        public static final int layout=0x7f070001;
        public static final int longitudeEditText=0x7f070003;
        public static final int obtainButton=0x7f070005;
        public static final int position=0x7f070002;
        public static final int sensorModeButton=0x7f07000b;
        public static final int settingButton=0x7f07000d;
        public static final int skyLayout=0x7f070007;
        public static final int starrySkyView=0x7f070009;
        public static final int testDisplay=0x7f07000a;
        public static final int textView2=0x7f070000;
        public static final int touchModeButton=0x7f07000c;
        public static final int useManualInputCheckBox=0x7f070006;
    }
    public static final class layout {
        public static final int about=0x7f030000;
        public static final int help=0x7f030001;
        public static final int setting=0x7f030002;
        public static final int sky=0x7f030003;
        public static final int test=0x7f030004;
        public static final int welcome=0x7f030005;
    }
    public static final class string {
        public static final int about=0x7f050002;
        public static final int app_name=0x7f050000;
        public static final int help=0x7f050001;
    }
    public static final class style {
        public static final int NoTitleDialog=0x7f060000;
    }
}
