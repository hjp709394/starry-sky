package com.starrysky.frame;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.starrysky.orientation.CoordinateSystem;
import com.starrysky.util.ToNativeOrderBuffer;

public class ConstellationLines implements SelfDrawable {

	private FloatBuffer mVerticesBuffer = null;
	
	private int mNumOfIndices = -1;

	private final float[] mStarColor = new float[] { 0.0f, 1.0f, 1.0f, 1.0f };
	
	public void setLines(float[] starPair) {
		if ((starPair.length % 6) != 0) {
			throw new IllegalArgumentException("starPair array's length must be 3*n");
		}
		
		int starCount = starPair.length / 3;
		
		// Vertex coordinates array
		float[] starsXYZ = new float[starCount * 3];
		float[] xyz = new float[]{ 1000, 0, 0 };
		float[] result = new float[3];
		for (int i = 0; i < starCount; i++) {
			int i3 = i * 3;
			CoordinateSystem.rotateByLongitudeAndLatitude(
					starPair[i3] * 15, 
					starPair[i3 + 1], 
					xyz, 
					result);
			System.arraycopy(result, 0, starsXYZ, i * 3, 3);
		}
		
		mVerticesBuffer = ToNativeOrderBuffer.toNativeOrderBuffer(starsXYZ);
		mNumOfIndices = starCount;
	}
	
	@Override
	public void selfDraw(GL10 gl) {
		if (mNumOfIndices <= 0) {
			return;
		}
		gl.glColor4f(mStarColor[0], mStarColor[1], mStarColor[2], mStarColor[3]);
		
//		gl.glEnable(GL10.GL_ALPHA_TEST);
//		gl.glAlphaFunc(GL10.GL_GREATER, 0.9f);
//		gl.glEnable(GL10.GL_BLEND);
//		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glColor4f(mStarColor[0], mStarColor[1], mStarColor[2], mStarColor[3]);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVerticesBuffer);

//		gl.glEnable(GL10.GL_TEXTURE_2D);
		// Enable the texture state
//		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		// Point to the texture buffers
//		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureCoordinatesBuffer);
//		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);

		// Draw the vertices
		gl.glDrawArrays(GL10.GL_LINES, 0, mNumOfIndices);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
//		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

//		gl.glDisable(GL10.GL_ALPHA_TEST);
//		gl.glDisable(GL10.GL_BLEND);
		// Not disabling texture would affect other frame's drawing
//		gl.glDisable(GL10.GL_TEXTURE_2D);
	}

	public void setColor(float red, float green, float blue, float alpha) {
		mStarColor[0] = red;
		mStarColor[1] = green;
		mStarColor[2] = blue;
		mStarColor[3] = alpha;
	}


	@Override
	public void loadGL(GL10 gl) {
		// No need to do anything 
	}

}
