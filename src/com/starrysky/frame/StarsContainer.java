package com.starrysky.frame;

import javax.microedition.khronos.opengles.GL10;

import com.starrysky.R;
import com.starrysky.util.LoadStarsFromFile;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;

public class StarsContainer implements SelfDrawable {
	
	private Stars stars;
	
	public StarsContainer(Context context) {
		Resources resources = context.getResources();
		stars = new Stars();
		stars.setTextureBitmap(BitmapFactory.decodeResource(resources, R.drawable.dot64));
		
		float[] starsArray = LoadStarsFromFile.loadStarsArray(context);
//		float[] starsArray = new float[]{
//				0.0f, 0.0f, 1.0f,
//				0.0f, 5.0f, 1.0f,
//				-5.0f, 0.0f, 1.0f,
//				6.0f, -34.0f, 1.0f,
//				0f, 15f, 2.0f,
//				12f, -5.1f, 1.0f,
//				13f, 29.1f, 1.0f,
//				6.0f, 63.0f, 1.0f};	// TEST
		stars.setStars(starsArray);
		
//		stars2 = new Stars2();
//		stars2.setColor(1, 0, 0, 1);
//		stars2.setTextureBitmap(BitmapFactory.decodeResource(resources, R.drawable.dot64));
//		
//		stars2.setStars(starsArray);
//		LoadStarsFromFile.deleteStarsArrayFile(context);
	}
	
	@Override
	public void selfDraw(GL10 gl) {
		stars.selfDraw(gl);
//		stars2.selfDraw(gl);
	}
	
	@Override
	public void loadGL(GL10 gl) {
		stars.loadGL(gl);
//		stars2.loadGL(gl);
	}

}
