package com.starrysky.frame;

import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;
import com.starrysky.math.Math3D;
import com.starrysky.util.ToNativeOrderBuffer;

public class CameraFrame extends AbstractFrame {

	{
		vForward[2] = -1;
	}
	
	/**
	 * Apply camera transform. Like gluLookAt at OpengGL.
	 * Camera move backward equals all actors move forward and
	 * Camera rotate angle surrounding X axis.
	 */
	@Override
	public void applyTransformation(GL10 gl) {
		float[] mMatrix = new float[4 * 4];
		float[] vAxisX = new float[3];
		float[] zFlipped = new float[3];
		float[] vResult = new float[3];

		zFlipped[0] = -vForward[0];
		zFlipped[1] = -vForward[1];
		zFlipped[2] = -vForward[2];

		// Derive X vector
		Math3D.crossProduct(vUp, zFlipped, vAxisX);

		// Populate matrix, note this is just the rotation and is transposed
		mMatrix[0] = vAxisX[0];
		mMatrix[4] = vAxisX[1];
		mMatrix[8] = vAxisX[2];
		mMatrix[12] = 0.0f;

		Math3D.getVerticalVector(vForward, vUp, vResult);
		Math3D.vectorAssign(vResult, vUp);
		
		mMatrix[1] = vUp[0];
		mMatrix[5] = vUp[1];
		mMatrix[9] = vUp[2];
		mMatrix[13] = 0.0f;

		mMatrix[2] = zFlipped[0];
		mMatrix[6] = zFlipped[1];
		mMatrix[10] = zFlipped[2];
		mMatrix[14] = 0.0f;

		mMatrix[3] = 0.0f;
		mMatrix[7] = 0.0f;
		mMatrix[11] = 0.0f;
		mMatrix[15] = 1.0f;

		// Do the rotation first
		gl.glMultMatrixf( (FloatBuffer) ToNativeOrderBuffer.toNativeOrderBuffer( mMatrix ));

		// Now, translate backwards
		gl.glTranslatef(-vLocation[0], -vLocation[1], -vLocation[2]);
	}

}
