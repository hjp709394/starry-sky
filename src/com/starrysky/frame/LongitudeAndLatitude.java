package com.starrysky.frame;

import java.nio.Buffer;
import javax.microedition.khronos.opengles.GL10;

import com.starrysky.util.ToNativeOrderBuffer;

public class LongitudeAndLatitude extends ActorFrame {
	
	private float r;					// radios of the celestial sphere
	
	private Buffer[] longitudeBuf;
	private Buffer[] latitudeBuf;
	
	private int longitudeStep 	= 15;		// draw a circle every longitudeStep
	private int latitudeStep 	= 15;		// draw a circle every latitudeStep
	private int lineStep 		= 15;		// draw a line every lineStep to 
											// make the polygon looks like a circle
	
	public LongitudeAndLatitude() {
		this(0.0f);
	}
	
	public LongitudeAndLatitude(float r) {
		this.r = r;
		initializeBuffers();
	}

	private void initializeBuffers() {
		latitudeBuf = new Buffer[180 / latitudeStep - 1];
		// points represented with longitude and latitude
		float points[][] = new float[ 360 / lineStep ][2];
		float[] xyz = new float[360 / lineStep * 3];
		for (int i = -90 + latitudeStep; i < 90; i += latitudeStep) {
			for (int j = 0; j < 360; j += lineStep) {
				points[j / lineStep][0] = (float) ( j * Math.PI / 180.0f );
				points[j / lineStep][1] = (float) ( i * Math.PI / 180.0f );
			}
			for (int j = 0; j < 360; j += lineStep) {
				int index = j / lineStep;
				int index3 = index * 3;
				xyz[index3 + 0] = (float) ( r * Math.cos(points[index][1]) * Math.cos(points[index][0]) );
				xyz[index3 + 1] = (float) ( r * Math.sin(points[index][1]) );
				xyz[index3 + 2] = (float) ( -r * Math.cos(points[index][1]) * Math.sin(points[index][0]) );
			}
			latitudeBuf[i / latitudeStep + 90 / latitudeStep - 1] = 
					ToNativeOrderBuffer.toNativeOrderBuffer(xyz);
		}
		
		// initialize longitude buffer
		longitudeBuf = new Buffer[360 / longitudeStep];
		xyz = new float[(180 / lineStep + 1) * 3];
		for (int i = 0; i < 360; i += longitudeStep) {
			float longitudeI = (float) (i * Math.PI / 180.0f);
			for (int j = -90; j <= 90; j += lineStep) {
				int index = j / lineStep + (90 / lineStep);
				points[index][0] = (float) longitudeI;
				points[index][1] = (float) ( j * Math.PI / 180.0f);
			}
			for (int j = -90; j <= 90; j += lineStep) {
				int index = j / lineStep + (90 / lineStep);
				int index3 = index * 3;
				xyz[index3 + 0] = (float) ( r * Math.cos(points[index][1]) * Math.cos(points[index][0]) );
				xyz[index3 + 1] = (float) ( r * Math.sin(points[index][1]) );
				xyz[index3 + 2] = (float) ( -r * Math.cos(points[index][1]) * Math.sin(points[index][0]) );
			}
			longitudeBuf[i / longitudeStep] = 
					ToNativeOrderBuffer.toNativeOrderBuffer(xyz);
		}
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		if (r < 0) {
			r = 0.0f;
		}
		this.r = r;
	}

	@Override
	public void selfDraw(GL10 gl) {
		gl.glColor4f(0.2f, 0.2f, 0.2f, 1.0f);			// color
		
		gl.glLineWidth(1);
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY); 	// enable vertex setting
		
		int pointCount;
		// latitude
		pointCount = 360 / lineStep;
		for (int i = 0; i < latitudeBuf.length; i++) {
			gl.glVertexPointer(3, GL10.GL_FLOAT, 0, latitudeBuf[i]);
			gl.glDrawArrays(GL10.GL_LINE_LOOP, 0, pointCount);
		}

		// longitude
		pointCount = 180 / lineStep + 1;
		for (int i = 0; i < longitudeBuf.length; i++) {
			gl.glVertexPointer(3, GL10.GL_FLOAT, 0, longitudeBuf[i]);
			gl.glDrawArrays(GL10.GL_LINE_LOOP, 0, pointCount);
		}
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
	}

	@Override
	public void loadGL(GL10 gl) {
		//Do nothing on purpose since no need to because here no texture is used.
	}

}
