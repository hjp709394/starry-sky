package com.starrysky.frame;

import javax.microedition.khronos.opengles.GL10;

public interface SelfDrawable {
	public void selfDraw(GL10 gl);
	
	// Some selfDrawble objects have textures, which should be reload 
	// when returning from the locking screen.
	public void loadGL(GL10 gl);
}
