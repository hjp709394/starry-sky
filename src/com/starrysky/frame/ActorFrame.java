package com.starrysky.frame;

import javax.microedition.khronos.opengles.GL10;
import com.starrysky.math.Math3D;
import com.starrysky.util.ToNativeOrderBuffer;

public abstract class ActorFrame 
		extends 	AbstractFrame 
		implements 	SelfDrawable {

	/**
	 * Apply actor transformation.
	 */
	@Override
	public void applyTransformation(GL10 gl) {
	    float[] mTransformation = new float[4 * 4];
	    Math3D.getMatrixFromAxis(vUp, vForward, vLocation, mTransformation);
	    gl.glMultMatrixf( ToNativeOrderBuffer.toNativeOrderBuffer( mTransformation ) );
	}

}
