package com.starrysky.frame;

import javax.microedition.khronos.opengles.GL10;

import com.starrysky.math.Math3D;

public abstract class AbstractFrame {

	float[] vLocation 	= { 0.0f, 0.0f, 0.0f };
	float[] vUp 		= { 0.0f, 1.0f, 0.0f };
	float[] vForward	= { 0.0f, 0.0f, 1.0f };

	public abstract void applyTransformation(GL10 gl);
	
	/** Rotate the frame by angle surrounding vector vUp,
	 *  which is the y axis
	 **/
	public void rotateLocalY(float angle) {
		float[] mRotation = new float[4 * 4];
		float[] vResult = new float[3];
		
		// if vUp is not vertical to vForward, make it vertical to it
		Math3D.getVerticalVector(vForward, vUp, vResult);
		//Math3D.vectorAssign(vResult, vUp);
		Math3D.normalize(vResult, vUp);
		
		Math3D.getRotationMatrix(angle, vUp, mRotation);
		Math3D.rotateVector(vForward, mRotation, vResult);
		//Math3D.vectorAssign(vResult, vForward);
		Math3D.normalize(vResult, vForward);
	}

	/** Rotate the frame by angle surrounding vector vRight,
	 *  which is the x axis
	 **/
	public void rotateLocalX(float angle) {
		float[] mRotation = new float[4 * 4];
		float[] vRight = new float[3];
		float[] vResult = new float[3];
		Math3D.crossProduct(vUp, vForward, vRight);
		Math3D.normalize(vRight, vRight);
		Math3D.getRotationMatrix(angle, vRight, mRotation);
		
		Math3D.rotateVector(vForward, mRotation, vResult);
		//Math3D.vectorAssign(vResult, vForward);
		Math3D.normalize(vResult, vForward);
		
		Math3D.rotateVector(vUp, mRotation, vResult);
		//Math3D.vectorAssign(vResult, vUp);
		Math3D.normalize(vResult, vUp);
	}
	
	/* not tested */
	public void rotateLocalZ(float angle) {
		float[] mRotation = new float[4 * 4];
		float[] vResult = new float[3];
		
		//float[] vForwardReverse = new float[3];
		//Math3D.scaleVector(-1, vForward, vForwardReverse);
		Math3D.getRotationMatrix(angle, vForward, mRotation);
		
		Math3D.rotateVector(vUp, mRotation, vResult);
		//Math3D.vectorAssign(vResult, vForward);
		Math3D.normalize(vResult, vUp);
		
		//Math3D.rotateVector(vUp, mRotation, vResult);
		//Math3D.vectorAssign(vResult, vUp);
		//Math3D.normalize(vResult, vUp);
	}

	public float[] getvLocation(float[] vResult) {
		Math3D.vectorAssign(vLocation, vResult);
		return vResult;
	}

	public void setvLocation(float x, float y, float z) {
		vLocation[0] = x;
		vLocation[1] = y;
		vLocation[2] = z;
	}

	public float[] getvUp(float[] vResult) {
		Math3D.vectorAssign(vUp, vResult);
		return vResult;
	}

	public void setvUp(float x, float y, float z) {
		vUp[0] = x;
		vUp[1] = y;
		vUp[2] = z;
	}

	public float[] getvForward(float[] vResult) {
		Math3D.vectorAssign(vForward, vResult);
		return vResult;
	}

	public void setvForward(float x, float y, float z) {
		vForward[0] = x;
		vForward[1] = y;
		vForward[2] = z;
	}

}