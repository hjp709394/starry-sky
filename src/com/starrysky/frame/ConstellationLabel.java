package com.starrysky.frame;

import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;
import com.starrysky.orientation.CoordinateSystem;
import com.starrysky.util.ToNativeOrderBuffer;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.opengl.GLUtils;

// TODO: Make labels look the same size with different view frustum
// using the information of frustum( glFrustum() )
// TODO: Make labels look horizontal use the vUp vector
public class ConstellationLabel implements SelfDrawable {
	
	public ConstellationLabel() {}
	
	public ConstellationLabel(float rightAscension, float declination, String label) {
		setLabel(rightAscension, declination, label);
	}

	private Bitmap bitmap;
	
	private static final float VERTICES_FACTOR = 0.5f;
	private static final float TEXT_DISTANCE = 1000;
	private final float[][] VERTICES = new float[][] {
//			 { -BITMAP_DEFAULT_SIZE * VERTICES_FACTOR 	, -TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, -TEXT_DISTANCE },
//			 { BITMAP_DEFAULT_SIZE * VERTICES_FACTOR	, -TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, -TEXT_DISTANCE },
//			 { -BITMAP_DEFAULT_SIZE * VERTICES_FACTOR	, TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, -TEXT_DISTANCE },
//			 { BITMAP_DEFAULT_SIZE * VERTICES_FACTOR	, TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, -TEXT_DISTANCE }
			 {  TEXT_DISTANCE	, -TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, -BITMAP_DEFAULT_SIZE * VERTICES_FACTOR	},
			 {  TEXT_DISTANCE	, -TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, BITMAP_DEFAULT_SIZE * VERTICES_FACTOR  	},
			 {  TEXT_DISTANCE	, TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, -BITMAP_DEFAULT_SIZE * VERTICES_FACTOR 	},
			 {  TEXT_DISTANCE	, TEXT_DEFAULT_SIZE * VERTICES_FACTOR	, BITMAP_DEFAULT_SIZE * VERTICES_FACTOR		}
	};
	
	private final float[] COORD = new float[]{
			0		, 1.0f	,
			1.0f	, 1.0f	,
			0		, 0		,
			1.0f	, 0		};
  
	private int[] textures = new int[1];	//纹理存储定义，一般用来存名称
	
	FloatBuffer vertexBuffer;  
	FloatBuffer coordBuffer;

	@Override
	public void selfDraw(GL10 gl) {
		gl.glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
		
		gl.glEnable(GL10.GL_ALPHA_TEST);
		gl.glAlphaFunc(GL10.GL_GEQUAL, 0.0f);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, coordBuffer);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glDisable(GL10.GL_BLEND);
		gl.glDisable(GL10.GL_ALPHA_TEST);
	}

	// If the string to be rendered is longer than 9 Chinese characters,
	// make the texture bitmap larger.
	private float rightAscension;
	private float declination;
	private String label;
	public void setLabel(float rightAscension, float declination, String label) {
		this.rightAscension = rightAscension;
		this.declination = declination;
		this.label = label;
		
		this.initLabel();
	}
	
	private void initLabel() {
		initTextureBitmap();
		initBuffer();
	}
	
	private static int BITMAP_DEFAULT_SIZE = 256;
	private static int TEXT_DEFAULT_SIZE = 32;
	private void initTextureBitmap() {
		bitmap = Bitmap.createBitmap(BITMAP_DEFAULT_SIZE, TEXT_DEFAULT_SIZE, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		canvas.drawColor(0x00000000);  //背景颜色：透明
		Paint p = new Paint();
		String fontType = null;
		Typeface typeface = Typeface.create(fontType, Typeface.BOLD);
		  
		p.setColor(Color.WHITE);	//TODO: text color modifiable
		p.setTypeface(typeface);
		p.setTextSize(TEXT_DEFAULT_SIZE);
		p.setTextAlign(Paint.Align.CENTER);
		
		// To adjust the position of the text so that it is positioned right.
		// See FontMetircs for more information.
		float verticalPositionAdjust = ( p.getFontMetrics().ascent + p.getFontMetrics().descent ) / 2;
		canvas.drawText(label, BITMAP_DEFAULT_SIZE / 2, TEXT_DEFAULT_SIZE / 2 - verticalPositionAdjust, p);  //绘制字体
	}
	
	private void initBuffer() {
		// Initialize texture coordinate buffer
		coordBuffer = ToNativeOrderBuffer.toNativeOrderBuffer(COORD);
		
		// Initialize vertices buffer
		float[] vertice = new float[3 * 4];
//		float[] vY = new float[]{0, 1, 0};
//		float[] vZ = new float[]{0, 0, 1};
//		float[] mRotationY = new float[16];
//		float[] mRotationZ = new float[16];
//		
//		float[] vResult1 = new float[3];
//		float[] vResult2 = new float[3];
//		
//		//float[] scaledVertices = new float[3];
//		
//		Math3D.getRotationMatrix( (float)(rightAscension / 24 * 2 * Math.PI), 
//				vY, mRotationY );
//		Math3D.rotateVector(vZ, mRotationY, vResult1);
//		Math3D.normalize(vResult1, vZ);
//		Math3D.getRotationMatrix( (float)(declination / 180 * Math.PI), 
//				vZ, mRotationZ);
//		
//		// Here, to rotate the vector twice is faster than 
//		// rotate it once using the combination rotation 
//		// matrix M = mRotationRight X mRotationUp
//		for (int i = 0; i < 4; ++i) {
//			Math3D.rotateVector(VERTICES[i], mRotationY, vResult1);
//			Math3D.rotateVector(vResult1, mRotationZ, vResult2);
//			System.arraycopy(vResult2, 0, vertice, i * 3, 3);
//		}
		
		float[] v = new float[12];
		for (int i = 0; i < 4; ++i) {
			System.arraycopy(VERTICES[i], 0, v, i * 3, 3);
		}
		CoordinateSystem.rotateByLongitudeAndLatitude (
				rightAscension * 15, 
				declination, 
				v, 
				vertice);
		
		vertexBuffer = ToNativeOrderBuffer.toNativeOrderBuffer(vertice);
	}
	
	public void loadGL(GL10 gl) {
		gl.glGenTextures(1, textures, 0);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,  
				GL10.GL_LINEAR);//放大时  
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,  
				GL10.GL_LINEAR);//缩小时
		
		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
	}

}
