package com.starrysky.frame;

import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public class ConstellationLabelsContainer implements SelfDrawable {
	
	public ConstellationLabelsContainer() {
		for (int i = 0; i < constellationNames.length; ++i) {
			this.addConstellationLabel(
					sphericalCoordinate[i * 2], sphericalCoordinate[i * 2 + 1], 
					constellationNames[i]);
		}
	}
	
	private static final int defaultCapasity = 88; 
	
	ArrayList<ConstellationLabel> c = new ArrayList<ConstellationLabel>(defaultCapasity);

	@Override
	public void selfDraw(GL10 gl) {
		for (ConstellationLabel cl : c) {
			cl.selfDraw(gl);
		}
	}
	
	@Override
	public void loadGL(GL10 gl) {
		for (ConstellationLabel cl : c) {
			cl.loadGL(gl);
		}
	}

	public void addConstellationLabel(float rightAscension, float declination, String label) {
		ConstellationLabel cl = new ConstellationLabel(rightAscension, declination, label);
		c.add(cl);
	}

	private static final String[] constellationNames = {
		"仙女座", "唧筒座", "天燕座", "水瓶座", "天鹰座", "天坛座", "白羊座", "御夫座", "牧夫座", "雕具座",
		"鹿豹座", "巨蟹座", "猎犬座", "大犬座", "小犬座", "摩羯座", "船底座", "仙后座", "半人马座", "仙王座",
		"鯨魚座", "蝘蜓座", "圆规座", "天鸽座", "后发座", "南冕座", "北冕座", "乌鸦座", "巨爵座", "南十字座",
		"天鹅座", "海豚座", "剑鱼座", "天龙座", "小马座", "波江座", "天炉座", "双子座", "天鹤座", "武仙座",
		"时钟座", "长蛇座", "水蛇座", "印第安座", "蝎虎座", "狮子座", "小狮座", "天兔座", "天秤座", "豺狼座",
		"天猫座", "天琴座", "山案座", "显微镜座", "麒麟座", "苍蝇座", "矩尺座", "南极座", "蛇夫座", "猎戶座",
		"孔雀座", "飞马座", "英仙座", "凤凰座", "绘架座", "双鱼座", "南鱼座", "船尾座", "罗盘座", "网罟座",
		"天箭座", "射手座", "天蝎座", "玉夫座", "盾牌座", "巨蛇座（头）", "巨蛇座（尾）", "六分仪座", "金牛座", "望远镜座", 
		"三角座", "南三角座", "杜鹃座", "大熊座", "小熊座", "船帆座", "处女座", "飞魚座", "狐狸座"};
	
	private static final float[] sphericalCoordinate = {
		0.666667f, 38,
		10f, -35,
		16f, -76,
		22.3333f, -13,
		19.5f, 2,
		17.1667f, -55,
		2.5f, 20,
		6f, 42,
		14.5833f, 30,
		4.83333f, -38,
		5.83333f, -38,
		8.5f, 20,
		13f, 40,
		6.66667f, -22,
		7.5f, 6,
		21f, -20,
		8.66667f, -62,
		1f, 60,
		13.3333f, -47,
		22f, 70,
		1.75f, -12,
		10.6667f, -78,
		14.8333f, -63,
		5.66667f, -34,
		12.6667f, 23,
		18.5f, -41,
		15.6667f, 32,
		12.3333f, -18,
		11.3333f, -15,
		12.3333f, -60,
		20.5f, 43,
		20.5833f, 12,
		5f, -60,
		17f, 60,
		21.1667f, 6,
		3.83333f, -30,
		2.75f, -33,
		7f, 22,
		22.3333f, -47,
		17.1667f, 27,
		3.33333f, -52,
		10.5f, -20,
		2.66667f, -72,
		21.3333f, -58,
		22.4167f, 43,
		10.5f, 15,
		10.3333f, 33,
		5.41667f, -20,
		15.1667f, -14,
		15f, -40,
		7.83333f, 45,
		18.75f, 36,
		5.66667f, -77,
		20.8333f, -37,
		7f, -3,
		12.5f, -70,
		16f, -50,
		21f, -87,
		17.1667f, -8,
		5.41667f, 3,
		19.1667f, -65,
		22.5f, 17,
		3.33333f, 42,
		1f, -48,
		5.5f, -52,
		0.333333f, 10,
		22.1667f, -32,
		7.66667f, -32,
		8.83333f, -28,
		3.83333f, -63,
		19.6667f, 18,
		19f, -25,
		16.75f, -26,
		0.5f, -35,
		18.6667f, -10,
		15.5833f, 8,
		18f, -5,
		10.1667f, -1,
		4.5f, 18,
		19f, -52,
		2f, 32,
		15.6667f, -65,
		23.75f, -68,
		11f, 58,
		15.6667f, 78,
		9.5f, -45,
		13.3333f, -2,
		7.66667f, -69,
		20.1667f, 25,
	};


}
