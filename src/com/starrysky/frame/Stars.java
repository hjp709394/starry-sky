package com.starrysky.frame;

import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;
import com.starrysky.orientation.CoordinateSystem;
import com.starrysky.util.ToNativeOrderBuffer;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.util.Log;

// TODO: Object-oriented data structure -- use class Star, Constellation, Sky to
//		 store the stars' information(position, magnitude, name). use another
//		 class StarDrawer, ConstellationDrawer, SkyDrawer to draw 
//		 according to the Star, Constellation and Sky
public class Stars implements SelfDrawable {
	
	private FloatBuffer mVerticesBuffer = null;
	private FloatBuffer mTextureCoordinatesBuffer = null;
	
	private int mTextureId = -1;

	private Bitmap mTextureBitmap;

	private int mNumOfIndices = -1;

	private final float[] mStarColor = new float[] { 0.0f, 1.0f, 0.0f, 1.0f };

    // Mapping texture using triangles whose edges is lines 
	// paralleled with the tangent line of the inner circle texture 
	// and the distance between the edges and the corresponding 
	// tangent lines is 0.1, which makes it looks normal when 
    // the parameter for GL_TEXTURE_WRAP_[ST] is set to 
	// CLAMP_TO_EDGE. In this way the data used for texture can be
	// reduced compared with mapping like a square tile.
	//
	// This type of mapping requires that there is space between 
	// the middle circle texture and the texture border. And the  
	// space in the outside of the circle texture must be 
	// transparent. The texture image's size must be 2^n.
	private static final float SQRT_3 = (float) Math.sqrt(3);
	private final float TEXTURE_COORDINATES[] = {
			0.5f					, 0.5f + 2 * 0.6f,
			-SQRT_3 * 0.6f + 0.5f	, -0.1f,
			0.5f + SQRT_3 * 0.6f	, -0.1f
	}; 
//	private static final float SQRT_5 = (float) Math.sqrt(5);
//	private final float TEXTURE_COORDINATES[] = {
//			0.5f,	1.0f,
//			0.0f, 	0.0f,
//			1.0f, 	0.0f
//	}; 
	
	private final static float MIN_STAR_MAGNITUDE = 0.0f;
	private final static float MAX_STAR_MAGNITUDE = 6.0f;
	
	// DOT_RADIUS and DISTANCE determines how large the stars look together
//	private final float[] DOT_RADIUS = new float[]{100f, 100f, 100f, 24f, 20f, 16f, 12f};	
	private final float DOT_FACTOR = 0.7f;
	private final float[] DOT_RADIUS = new float[]{
			21f * DOT_FACTOR, 
			18f * DOT_FACTOR, 
			15f * DOT_FACTOR, 
			12f * DOT_FACTOR, 
			9f * DOT_FACTOR, 
			6f * DOT_FACTOR, 
			4f * DOT_FACTOR};	
	
	// TODO: setting proper DISTANCE instead of hard code it
	// DISTANCE should be the same as the radius of celestial sphere
	private static final float DISTANCE = 1000.0f;
	private final float[][] VERTICES = new float[][] {
			// 0 right ascension across x axis
			{DISTANCE	, 2		, 0.0f},
			{DISTANCE	, -1	, -SQRT_3},
			{DISTANCE	, -1	, +SQRT_3}
	};
//	private final float[][] VERTICES = new float[][] {
//			// 0 right ascension across x axis
//			{DISTANCE	, 1 - SQRT_5	, 0								},
//			{DISTANCE	, -0.5f			, -(1.0f / (1.0f + SQRT_5)) 	},
//			{DISTANCE	, -0.5f			, (1.0f / (1.0f + SQRT_5))		}
//	};
	
	/**
	 * Add stars out of the position data stored in a float array.
	 * @param stars
	 * stars[3*n] stores the Right Ascension of the nth star, 
	 * stars[3*n + 1] stores the Declination of the nth star,
	 * stars[3*n + 2] stores the magnitude of the nth star.
	 */
	public void setStars(float[] stars) {
		
		if ((stars.length % 3) != 0) {
			throw new IllegalArgumentException("stars array's length must be 3*n");
		}
		
		int starCount = stars.length / 3;
		float[] starsXYZ = new float[starCount * 3 * 3];	// right(or x) axis is cross 0 right ascension 
		float[] textureCoordinates = new float[starCount * 6];
		
		// Texture coordinates array
		for (int i = 0; i < starCount; i++) {
			System.arraycopy(TEXTURE_COORDINATES, 0, textureCoordinates, 
					i * TEXTURE_COORDINATES.length, TEXTURE_COORDINATES.length);
		}
		
		// Vertex coordinates array
		float[] scaledVertices = new float[9];
		float[] result = new float[9];
		for (int i = 0; i < starCount; i++) {	
			int i3 = i * 3;
			int starMagnitude = Math.round( Math.max(
					Math.min( stars[i3 + 2], MAX_STAR_MAGNITUDE ), 
					MIN_STAR_MAGNITUDE ) );
			for (int j = 0; j < 3; ++j) {
				int j3 = j * 3;
				scaledVertices[j3 + 0] = VERTICES[j][0];
				scaledVertices[j3 + 1] = VERTICES[j][1] * DOT_RADIUS[starMagnitude];
				scaledVertices[j3 + 2] = VERTICES[j][2] * DOT_RADIUS[starMagnitude];
			}
			CoordinateSystem.rotateByLongitudeAndLatitude(
					stars[i3] * 15, 
					stars[i3 + 1], 
					scaledVertices, 
					result);
			System.arraycopy(result, 0, starsXYZ, i * 9, 9);
		}
//		Log.i("Stars", String.format("magnitude: %d %d %d %d %d %d %d",	magnitudeCount[0], magnitudeCount[1], magnitudeCount[2], magnitudeCount[3],	magnitudeCount[4], magnitudeCount[5], magnitudeCount[6] ) );
		
		mTextureCoordinatesBuffer = ToNativeOrderBuffer.toNativeOrderBuffer(textureCoordinates);
		mVerticesBuffer = ToNativeOrderBuffer.toNativeOrderBuffer(starsXYZ);
		mNumOfIndices = starCount * 3;
	}
	
	@Override
	public void selfDraw(GL10 gl) {
		if (mNumOfIndices <= 0) {
			return;
		}
		gl.glColor4f(mStarColor[0], mStarColor[1], mStarColor[2], mStarColor[3]);
		
		gl.glEnable(GL10.GL_ALPHA_TEST);
		gl.glAlphaFunc(GL10.GL_GREATER, 0.9f);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVerticesBuffer);
		gl.glColor4f(mStarColor[0], mStarColor[1], mStarColor[2], mStarColor[3]);

		gl.glEnable(GL10.GL_TEXTURE_2D);
		
		// Enable the texture state
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		// Point to the texture buffers
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureCoordinatesBuffer);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);

		// Draw the vertices
		gl.glDrawArrays(GL10.GL_TRIANGLES, 0, mNumOfIndices);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		gl.glDisable(GL10.GL_ALPHA_TEST);
		gl.glDisable(GL10.GL_BLEND);
		// Not disabling texture would affect other frame's drawing
		gl.glDisable(GL10.GL_TEXTURE_2D);
	}

	public void setColor(float red, float green, float blue, float alpha) {
		mStarColor[0] = red;
		mStarColor[1] = green;
		mStarColor[2] = blue;
		mStarColor[3] = alpha;
	}

	public void setTextureBitmap(Bitmap bitmap) {
		this.mTextureBitmap = bitmap;
	}

	public void loadGLTexture(GL10 gl) {
		int[] textures = new int[1];
		gl.glGenTextures(1, textures, 0);
		mTextureId = textures[0];

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);

		// GL_CLAMP_TO_EDGE is necessary since we render a point with 
		// triangle texture coordinate mapping.
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
				GL10.GL_CLAMP_TO_EDGE);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
				GL10.GL_CLAMP_TO_EDGE);

		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, mTextureBitmap, 0);
	}

	// Load every thing about opengl es such as texture.
	// Notice: the texture would be destroyed when you return from 
	// screen locking, so call this method in the GLSurfaceView#onSurfaceChange
	// to load gl data like texture.
	public void loadGL(GL10 gl) {
		loadGLTexture(gl);
	}

}
