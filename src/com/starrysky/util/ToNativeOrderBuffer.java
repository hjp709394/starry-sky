package com.starrysky.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public class ToNativeOrderBuffer {

	// TODO: Add argument startIndex and count to 
	// put a subset of the total array to the buffer
	public static IntBuffer toNativeOrderBuffer(int []arr){  
		IntBuffer mBuffer;

		//先初始化buffer,数组的长度*4,因为一个int占4个字节
		ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length * 4);
		//数组排列用nativeOrder
		qbb.order(ByteOrder.nativeOrder());

		mBuffer = qbb.asIntBuffer();
		mBuffer.put(arr);
		mBuffer.position(0);

		return mBuffer;
	}

	public static FloatBuffer toNativeOrderBuffer(float []arr){  
		FloatBuffer mBuffer;

		//先初始化buffer,数组的长度*4,因为一个float占4个字节
		ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length * 4);
		//数组排列用nativeOrder
		qbb.order(ByteOrder.nativeOrder());

		mBuffer = qbb.asFloatBuffer();
		mBuffer.put(arr);
		mBuffer.position(0);

		return mBuffer;  
	}

	public static DoubleBuffer toNativeOrderBuffer(double []arr){  
		DoubleBuffer mBuffer;

		//先初始化buffer, 一个double占8个字节
		ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length * 8);
		//数组排列用nativeOrder
		qbb.order(ByteOrder.nativeOrder());

		mBuffer = qbb.asDoubleBuffer();
		mBuffer.put(arr);
		mBuffer.position(0);

		return mBuffer;  
	}

	public static ShortBuffer toNativeOrderBuffer(short[] arr){  
		ShortBuffer mBuffer;

		ByteBuffer qbb = ByteBuffer.allocateDirect(arr.length * 2);
		//数组排列用nativeOrder
		qbb.order(ByteOrder.nativeOrder());

		mBuffer = qbb.asShortBuffer();
		mBuffer.put(arr);
		mBuffer.position(0);

		return mBuffer;
	}
	
	//byte[], char[], long[], short[] ...

}
