package com.starrysky.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import android.content.Context;
import android.util.Log;

public class LoadStarsFromFile {

	private final static int ARRAY_LENGTH = 12831;
	private final static String starsArrayFileName = "stars";
	private final static String starsFileName = "starsPosition.txt";
	public static float[] loadStarsArray(Context context) {
		//ObjectStorage.deleteObject(context, starsArrayFileName);
		
		// For fast speed, stars array was store in file system 
		// when the application first run. Now load it if exists. 
		float[] starsArray = (float[]) ObjectStorage.loadObject(context, starsArrayFileName);
		if (starsArray != null) {
			Log.i("loadStarsArray", "from file system");
			return starsArray;
		}
		
		// The stars array object not stored in the file system.
		// So load the stars array from assets.
		float[] arr = new float[ARRAY_LENGTH];
		int valCount = 0;
		try {
			InputStream is;
			is = context.getAssets().open(starsFileName);
			BufferedInputStream bis = new BufferedInputStream(is);
			StringBuffer sb = new StringBuffer();
			String s;
			int ch;
			float val;
			while ( ( ch = bis.read() ) != -1) {
				if ( Character.isWhitespace((char)ch) ) {
					continue;
				}
				sb.delete(0, sb.length());
				do {
					sb.append((char)ch);
					ch = bis.read();
				} while (ch != -1 && !Character.isWhitespace((char)ch) );
				s = sb.toString();
				if (!isFloatValueString(s)) {
					Log.i("test", s);
					continue;
				}
				val = Float.parseFloat(sb.toString());
				
				arr[valCount] = val;
				if (valCount % 3 == 0) {
					arr[valCount] *= 24f / 360f;
				}
				valCount++;
			}
			Log.i("loadStarsFromFile", "valCount: " + valCount);
			
			// Store the stars array for fast speed in the future 
			ObjectStorage.storeObject(context, starsArrayFileName, arr);
			
			Log.i("loadStarsArray", "from assets");
			return arr;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Log.i("loadStarsArray", "failed");
		return null;
	}
	
	public static void deleteStarsArrayFile(Context context) {
		String[] fileList = context.fileList();
		for (int i = 0; i < fileList.length; ++i) {
			if (!fileList[i].equals(starsArrayFileName)) {
				continue;
			} else {
				context.deleteFile(starsArrayFileName);
				break;
			}
		}
	}
	
	private static boolean isFloatValueString(String s) {
		int i = 0;
		if (s.length() == 0) {
			return false;
		}
		
		if (s.charAt(i) == '+' || s.charAt(i) == '-') {
			i++;
		}
		
		while (i < s.length() && s.charAt(i) >= '0' && s.charAt(i) <= '9') {
			i++;
		}
		
		if (i < s.length()) {
			if (s.charAt(i) == '.') {
				i++;
			} else {
				return false;
			}
		}
		
		while (i < s.length() && s.charAt(i) >= '0' && s.charAt(i) <= '9') {
			i++;
		}
		
		if (i == s.length()) {
			return true;
		}
		return false;
	}
}
