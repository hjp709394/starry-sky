package com.starrysky.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import android.content.Context;

/*
 * utility used to store states object like history or Grid
 */
public class ObjectStorage {
	
	public static boolean storeObject(Context context, String fileName, Serializable serializable) {
		try {
			OutputStream os = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(serializable);
			oos.close();
			return true;
			//os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static Object loadObject(Context context, String fileName) {
		String[] fileList = context.fileList();
		try {
			for (int i = 0; i < fileList.length; ++i) {
				if (fileList[i].equals(fileName)) {
					InputStream is = context.openFileInput(fileName);
					ObjectInputStream ois = new ObjectInputStream(is);
					// BUG: stream not closed 
					Object ret = ois.readObject();
					ois.close();
					return ret;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean deleteObject(Context context, String fileName) {
		String[] fileList = context.fileList();
		for (int i = 0; i < fileList.length; ++i) {
			if (fileList[i].equals(fileName)) {
				context.deleteFile(fileName);
				return true;
			}
		}
		return false;
	}

}
