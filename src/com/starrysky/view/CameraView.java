package com.starrysky.view;

import java.io.IOException;
import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class CameraView extends SurfaceView implements Callback {
	
    private Camera camera;
    
	public CameraView(Context context, AttributeSet attrs) {
		super(context, attrs);
        // We're implementing the Callback interface and want to get notified
        // about certain surface events.
        getHolder().addCallback( this );
        // We're changing the surface to a PUSH surface, meaning we're receiving
        // all buffer data from another component - the camera, in this case.
        getHolder().setType( SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS );
	}

	public CameraView( Context context ) {
        this( context, null );
    }
 
    public void surfaceCreated( SurfaceHolder holder ) {
        // Once the surface is created, simply open a handle to the camera hardware.
        camera = Camera.open();
        
        // NULL POINT EXCEPTION inside the getHorizontalViewAngle() method
        //Log.i("CameraView", "" + camera.getParameters().getHorizontalViewAngle());
    }
 
    // This method is called when the surface changes, e.g. when it's size is set.
    // We use the opportunity to initialize the camera preview display dimensions.
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ) {
    	// If not working with landscape mode, normally 
    	// the camera preview would be rotated by -90 degrees.
    	// So here rotate it back.
        if (this.getResources().getConfiguration().orientation 
        		!= Configuration.ORIENTATION_LANDSCAPE) {
        	// It is said this method only works on android whose version is
        	// above 2.1. I only have android 2.3 tested.
            camera.setDisplayOrientation(90);
        }
        
        Camera.Parameters p = camera.getParameters();
        p.setPreviewSize( width, height );
        //camera.setParameters( p );	// HJP:DELETE:2013.10.13:Setting this may cause failure
 
        // We also assign the preview display to this surface...
        try {
            camera.setPreviewDisplay( holder );
        } catch( IOException e ) {
            e.printStackTrace();
        }
        // Start previewing. From now on, the camera keeps pushing preview
        // images to the surface.
        camera.startPreview();
    }
 
    public void surfaceDestroyed( SurfaceHolder holder ) {
        // Once the surface gets destroyed, we stop the preview mode and release
        // the whole camera since we no longer need it.
        camera.stopPreview();
        camera.release();
        camera = null;
    }
}