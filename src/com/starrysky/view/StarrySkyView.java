package com.starrysky.view;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import com.starrysky.frame.AbstractFrame;
import com.starrysky.frame.CameraFrame;
import com.starrysky.frame.ConstellationLabelsContainer;
import com.starrysky.frame.ConstellationLinesContainer;
import com.starrysky.frame.LongitudeAndLatitude;
import com.starrysky.frame.StarsContainer;
import com.starrysky.orientation.SiderealTime;
import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.AttributeSet;
import android.util.Log;

public class StarrySkyView extends GLSurfaceView {
	
	StarrySkyRenderer renderer;
	
	public StarrySkyView(Context context) {
		this(context, null);
	}

	public StarrySkyView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
//		setEGLConfigChooser( 8, 8, 8, 8, 0, 0 );		// Not depth information needed
		getHolder().setFormat( PixelFormat.TRANSLUCENT );
		
		renderer = new StarrySkyRenderer();
		setRenderer(renderer);
		setRenderMode(RENDERMODE_WHEN_DIRTY);
		
		// It would cause invalid value for some specific phones.
		// so comment it when publish.
		// TODO: comment debug
//		setDebugFlags(DEBUG_CHECK_GL_ERROR);	// For easier debug
	}

	public void rotateView(float angleX, float angelY) {
		renderer.rotateX(angleX);
		renderer.rotateY(angelY);
		requestRender();	// RENDERMODE_WHEN_DIRTY
	}
	
	public void lookAt(float[] forward, float[] up) {
		renderer.lookAt(forward, up);
	}
	
	private class StarrySkyRenderer implements Renderer {

		private AbstractFrame camera;// = new CameraFrame();
		private LongitudeAndLatitude lgtltt;// = new LongitudeLatitude(100.0f);
		private StarsContainer starsContainer;
		private ConstellationLabelsContainer lbl;
		private ConstellationLinesContainer lns;

		@Override
		public void onDrawFrame(GL10 gl) {
			//Log.i("StarrySkyView", "draw");
			gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
			gl.glLoadIdentity();

			gl.glPushMatrix();

			// setting camera orientation
//			camera.applyTransformation(gl);
			
			// Another way to set camera orientation, which is easier
			// to switch between two operation modes: Sensor / Touch.
			float[] vUp = new float[3];
			float[] vForward = new float[3];
			camera.getvUp(vUp);
			camera.getvForward(vForward);
			GLU.gluLookAt(gl	, 
					0			, 0				, 0				, 
					vForward[0]	, vForward[1]	, vForward[2]	, 
					vUp[0]		, vUp[1]		, vUp[2]		);

			// Now draw the celestial sphere
			// Rotate the celestial sphere so that the zenith are on the top of your position
			// TODO: If getting UTC time is resources consuming operation, 
			// update it regularly instead of retrieving it every time
			// TODO: in touch mode, no need to consider sidereal time
			float siderealTime = (float) SiderealTime.getMeridianSiderealTime();
			gl.glRotatef(-siderealTime / 24 * 360, 0, 1, 0);
			
			// draw longitude and latitude
			lgtltt.selfDraw(gl);
			
			lbl.selfDraw(gl);
			
			lns.selfDraw(gl);

			// Draw stars. It is strange that if draw stars before constellation
			// label, some stars' color would be rendered the same as the labels. 
			starsContainer.selfDraw(gl);

			gl.glPopMatrix();
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {
			float factor = 0.9f;
			float ratio = (float) width / height * factor;
//			Log.i("StarrySkyView", String.format("width, height: %d %d", width, height));

			gl.glViewport(0, 0, width, height);
			
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			
			// TODO: If getting camera angle is possible, 
			// make it possible to modify the frustum so that 
			// the camera preview and the stars mix together totally
			gl.glFrustumf(-ratio, ratio, -factor, factor, 2.0f, 2000.0f);
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
			
			// Load gl data such as texture
			starsContainer.loadGL(gl);
			lbl.loadGL(gl);
			lns.loadGL(gl);
//			lgtltt.loadGL(gl);
		}

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			gl.glClearColor(0, 0, 0, 0);

			// Enable it when needed. Here disable Textre_2D for fast speed
			gl.glDisable( GL10.GL_TEXTURE_2D ); 

			// Initializing fields
			camera = new CameraFrame();
			
			lgtltt = new LongitudeAndLatitude(1000.0f);
			
			starsContainer = new StarsContainer(StarrySkyView.this.getContext());
			
			lbl = new ConstellationLabelsContainer();
			
			lns = new ConstellationLinesContainer(StarrySkyView.this.getContext()); 
		}

		public void rotateY(float angle) {
			camera.rotateLocalY(angle);
		}

		public void rotateX(float angle) {
			camera.rotateLocalX(angle);
		}
		
		@SuppressWarnings("unused")
		public void rotateZ(float angle) {
			camera.rotateLocalZ(angle);
		}
		
		public void lookAt(float[] forward, float[] up) {
//			Log.i("StarrySkyView", "lookat");
			if (forward == null || up == null || camera == null) {
				return;
			}
			camera.setvForward(forward[0], forward[1], forward[2]);
			camera.setvUp(up[0], up[1], up[2]);
			
			requestRender();
		}
	}

}
