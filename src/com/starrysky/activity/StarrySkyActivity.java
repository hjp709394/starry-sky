package com.starrysky.activity;

import com.starrysky.R;
import com.starrysky.orientation.CoordinateSystem;
import com.starrysky.orientation.MobileLocation;
import com.starrysky.orientation.OrientationEventListener;
import com.starrysky.orientation.OrientationEventListener.OnOrientationChangedListener;
import com.starrysky.view.CameraView;
import com.starrysky.view.StarrySkyView;
import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class StarrySkyActivity extends Activity {
	
	private SensorManager mSensorManager;
	
	private Sensor mAccelerometer;
	private Sensor mMegneticField;

	private StarrySkyView starrySkyView;
	
	private CameraView cameraView;	// Currently not used.
	
	public static final int TOUCH = 0;
	public static final int SENSOR = 1;
	private int mode = SENSOR;
	
	private StarrySkyOnTouchListener tl;
	private OrientationEventListener ol;
	private StarryOnOrientationChangedListener ocl;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    // When working with the camera, it's useful to stick to one orientation.
        // If not using Landscape, the StarrySkyView would not be seen when returns from locked screen. 
        // When orientation is changed, the Activity would be destroyed and recreate.
        // Comment: Use layout xml to set screen orientation.
//	    setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE );
	    
        // Comment: Setting full screen feature in the xml
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//        		WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
//		setContentView(R.layout.sky);
//		setContentView(R.layout.test);
		
//		starrySkyView = (StarrySkyView) findViewById(R.id.starrySkyView);
//		cameraView = (CameraView) findViewById(R.id.cameraView);
        
        // Now the glSurfaceView would not be disappeared from locking screen.
        cameraView = new CameraView(this);
        setContentView(cameraView, new LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

        starrySkyView = new StarrySkyView(this);
        addContentView(starrySkyView, new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        starrySkyView.setZOrderMediaOverlay(true);

        FrameLayout layout = new FrameLayout(this);
        addContentView(layout, new LayoutParams(
        		LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        layout.setVisibility(View.VISIBLE);
    
		// Initialize sensors
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mMegneticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		
		// Touch Mode
		tl = new StarrySkyOnTouchListener();
		
		// Sensor Mode
		ol = new OrientationEventListener();
		ocl = new StarryOnOrientationChangedListener();
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		ol.setOnOrientationChangedListener( ocl );
		
		mode = pref.getInt(
				PreferencesMap.getKey(PreferencesMap.OPERATION_MODE), 
				(Integer) PreferencesMap.getDefaultValue(PreferencesMap.OPERATION_MODE));
		// Obtaining position via base station
		if (!pref.getBoolean(
				PreferencesMap.getKey(PreferencesMap.USE_MANUAL_INPUT), 
				(Boolean) PreferencesMap.getDefaultValue(PreferencesMap.USE_MANUAL_INPUT))
				&& mode == SENSOR) {
			GetLocationTask t = new GetLocationTask();
			t.execute();
		}
    }
	
	@Override
	protected void onResume() {
		super.onResume();
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(StarrySkyActivity.this);
		mode = pref.getInt(
				PreferencesMap.getKey(PreferencesMap.OPERATION_MODE), 
				(Integer) PreferencesMap.getDefaultValue(PreferencesMap.OPERATION_MODE));
//		Log.i("StarrySkyActivity", "resume, mode: " + mode);
		if (mode == SENSOR) {
			cameraView.setVisibility(View.VISIBLE);
			if (isUsingManualInput 
					|| pref.getBoolean(
							PreferencesMap.getKey(PreferencesMap.USE_MANUAL_INPUT), 
							(Boolean) PreferencesMap.getDefaultValue(PreferencesMap.USE_MANUAL_INPUT))) {
				ocl.setPosition(
						pref.getFloat(PreferencesMap.getKey(PreferencesMap.LONGITUDE), 
								(Float) PreferencesMap.getDefaultValue(PreferencesMap.LONGITUDE)),
						pref.getFloat(PreferencesMap.getKey(PreferencesMap.LATITUDE), 
								(Float) PreferencesMap.getDefaultValue(PreferencesMap.LATITUDE)),
						0);
			}
			boolean registerSuccessful = mSensorManager.registerListener(
					ol, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
//			Log.i("StarrySkyActivity", "registered: " + registerSuccessful);
			registerSuccessful = mSensorManager.registerListener(
					ol, mMegneticField, SensorManager.SENSOR_MAGNETIC_FIELD);
//			Log.i("StarrySkyActivity", "registered: " + registerSuccessful);
			starrySkyView.setOnTouchListener(null);
		} else {
			cameraView.setVisibility(View.INVISIBLE);
			starrySkyView.setOnTouchListener(tl);
			mSensorManager.unregisterListener(ol, mAccelerometer);
			mSensorManager.unregisterListener(ol, mMegneticField);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(ol, mAccelerometer);
		mSensorManager.unregisterListener(ol, mMegneticField);
	}
	
//	private static final int MENU_SETTING = 0;
//	private static final int MENU_HELP = 1;
//	private static final int MENU_ABOUT= 2;
//	
//	@Override 
//	public boolean onCreateOptionsMenu(Menu menu) { 
//	    int groupId = 0;
//	    int menuOrder = Menu.NONE;
//	    menu.add(groupId, MENU_SETTING, menuOrder, "Setting"); 
//	    menu.add(groupId, MENU_HELP, menuOrder, "Help"); 
//	    menu.add(groupId, MENU_ABOUT, menuOrder, "About"); 
//	 
//	    return true;
//	}
	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		Intent intent = new Intent();
//		switch (item.getItemId()) {
//		case MENU_SETTING:
//			intent.setClass(
//					StarrySkyActivity.this, SettingActivity.class);
//			startActivity(intent);
//			break;
//		case MENU_HELP:
//			intent.setClass(
//					StarrySkyActivity.this, HelpActivity.class);
//			startActivity(intent);
//			break;
//		case MENU_ABOUT:
//			intent.setClass(
//					StarrySkyActivity.this, AboutActivity.class);
//			startActivity(intent);
//			break;
//		default:
//			break;
//		}
//		return super.onOptionsItemSelected(item);
//	}

	// Whether if using manual input position.
	private boolean isUsingManualInput = false;
	private class GetLocationTask extends AsyncTask<Void, Boolean, float[]> {
		@Override
		protected float[] doInBackground(Void ... params) {
			float[] position = new float[3];
			boolean isLocationObtained = 
					MobileLocation.getLocation(StarrySkyActivity.this, position);
			if (!isLocationObtained) {
				return null;
			}
			return position;
		}

		@Override
		protected void onPostExecute(float[] result) {
			if (result == null) {
				Toast.makeText(
						StarrySkyActivity.this, 
						"Location not obtained, using manual input.", 
						Toast.LENGTH_LONG).show();
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(StarrySkyActivity.this); 
				ocl.setPosition(
						pref.getFloat(PreferencesMap.getKey(PreferencesMap.LONGITUDE), 
								(Float) PreferencesMap.getDefaultValue(PreferencesMap.LONGITUDE)),
						pref.getFloat(PreferencesMap.getKey(PreferencesMap.LATITUDE), 
								(Float) PreferencesMap.getDefaultValue(PreferencesMap.LATITUDE)),
						0);
				isUsingManualInput = true;
			} else {
				ocl.setPosition(result[0], result[1], result[2]);
				isUsingManualInput = false;
			}
		}
	}
	
	private class StarryOnOrientationChangedListener implements OnOrientationChangedListener {
		
		public void setPosition(float longitude, float latitude, float accuracy) {
			lgtltt[0] = longitude;
			lgtltt[1] = latitude;
			lgtltt[2] = accuracy;
		}
		
		private float[] lgtltt = new float[3];
		
		@Override
		public void onOrientationChanged(float[] eastNorthVertical) {
//			Log.i("StarrySkyActivity", "orientation changed");
			
			float[] xyz = new float[9];
			
			CoordinateSystem.horizontalToEarth( eastNorthVertical, xyz, 
					lgtltt[0], lgtltt[1]);
			float[] forward = new float[3];
			float[] up = new float[3];
			
			System.arraycopy(xyz, 0, up, 0, 3);
			forward[0] = -xyz[6];
			forward[1] = -xyz[7];
			forward[2] = -xyz[8];
			
			starrySkyView.lookAt(forward, up);
		}
	
	}
	
	private class StarrySkyOnTouchListener implements OnTouchListener {

		private float mPreviousX;
		private float mPreviousY;
		
		@Override
		public boolean onTouch(View v, MotionEvent e) {
			float x = e.getX();
			float y = e.getY();

			switch (e.getAction()) {
			case  MotionEvent.ACTION_MOVE:
				float dx = +x - mPreviousX;		// x increase from left to right
				float dy = -y + mPreviousY;		// y increase from top to buttom
				float rotateX = (float) Math.atan(dy / starrySkyView.getHeight());
				float rotateY = (float) Math.atan(dx / starrySkyView.getWidth());
				starrySkyView.rotateView(rotateX, rotateY);
				starrySkyView.requestRender();
				break;
			default:
				break;
			}
			mPreviousX = x;
			mPreviousY = y;
			
			return true;
		}
	}
}

