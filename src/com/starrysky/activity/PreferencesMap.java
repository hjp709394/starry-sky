package com.starrysky.activity;

public class PreferencesMap {
	
//	public static int SKIP_WELCOME_PAGE = 0;
	public static final int OPERATION_MODE = 0;
	
	public static final int LONGITUDE = 1;
	public static final int LATITUDE = 2;
	
	public static final int USE_MANUAL_INPUT = 3; 
	
	private static String[] key = {
//		"SkipWelcomePage",
		"OperationMode",
		"Longitude",
		"Latitude",
		"UseManualInput"
	};
	
	private static Object[] defaultValue = {
//		false,
		StarrySkyActivity.SENSOR,
		0.0f,
		0.0f,
		false
	};
	
	public static String getKey(int preferenceValue) {
		if (preferenceValue < key.length) {
			return key[preferenceValue];
		} else {
			return null;
		}
	}
	
	public static Object getDefaultValue(int preferenceValue) {
		if (preferenceValue < key.length) {
			return defaultValue[preferenceValue];
		} else {
			return null;
		}
	}

}
