package com.starrysky.activity;

import com.starrysky.R;
import com.starrysky.orientation.MobileLocation;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		
		EditText longitudeEditText = (EditText) findViewById(R.id.longitudeEditText);
		EditText latitudeEditText = (EditText) findViewById(R.id.latitudeEditText);
		Button obtainButton = (Button) findViewById(R.id.obtainButton);
		CheckBox useManualInputCheckBox = (CheckBox) findViewById(R.id.useManualInputCheckBox);
		
		obtainButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				GetLocationTask t = new GetLocationTask();
				t.execute();
			}
		});
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(SettingActivity.this);
		
		useManualInputCheckBox.setChecked(pref.getBoolean(
				PreferencesMap.getKey(PreferencesMap.USE_MANUAL_INPUT), 
				(Boolean) PreferencesMap.getDefaultValue(PreferencesMap.USE_MANUAL_INPUT)));
		useManualInputCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(SettingActivity.this);
				Editor editor = pref.edit();
				editor.putBoolean(PreferencesMap.getKey(PreferencesMap.USE_MANUAL_INPUT), isChecked);
				editor.commit();
			}
		});
		
		longitudeEditText.setText( String.valueOf(
				pref.getFloat(PreferencesMap.getKey(PreferencesMap.LONGITUDE), 
						(Float) PreferencesMap.getDefaultValue(PreferencesMap.LONGITUDE)) ) );
		latitudeEditText.setText( String.valueOf(
				pref.getFloat(PreferencesMap.getKey(PreferencesMap.LATITUDE), 
						(Float) PreferencesMap.getDefaultValue(PreferencesMap.LATITUDE)) ) );
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		EditText longitudeEditText = (EditText) findViewById(R.id.longitudeEditText);
		EditText latitudeEditText = (EditText) findViewById(R.id.latitudeEditText);
		
		String longitudeString = longitudeEditText.getText().toString();
		String latitudeString = latitudeEditText.getText().toString();
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(SettingActivity.this);
		Editor editor = pref.edit();
		editor.putFloat(PreferencesMap.getKey(PreferencesMap.LONGITUDE), Float.parseFloat(longitudeString));
		editor.putFloat(PreferencesMap.getKey(PreferencesMap.LATITUDE), Float.parseFloat(latitudeString));
		editor.commit();
	}

	private class GetLocationTask extends AsyncTask<Void, Boolean, float[]> {
		@Override
		protected float[] doInBackground(Void ... params) {
			float[] position = new float[3];
			boolean isLocationObtained = 
					MobileLocation.getLocation(SettingActivity.this, position);
			if (!isLocationObtained) {
				return null;
			}
			return position;
		}

		@Override
		protected void onPostExecute(float[] result) {
			if (result == null) {
				Toast.makeText(
						SettingActivity.this, 
						"Location cannot be obtained. Please check your mobile network access", 
						Toast.LENGTH_LONG).show();
			} else {
				EditText longitudeEditText = (EditText) SettingActivity.this.findViewById(R.id.longitudeEditText);
				EditText latitudeEditText = (EditText) SettingActivity.this.findViewById(R.id.latitudeEditText);
				longitudeEditText.setText(String.valueOf(result[0]));
				latitudeEditText.setText(String.valueOf(result[1]));
			}
		}
	}
}
