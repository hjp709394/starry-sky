package com.starrysky.activity;

import com.starrysky.R;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
//		if (pref.getBoolean(
//				PreferencesMap.getKey(PreferencesMap.SKIP_WELCOME_PAGE), 
//				(Boolean) PreferencesMap.getDefaultValue(PreferencesMap.SKIP_WELCOME_PAGE))) {
//			Intent intent = new Intent(
//					WelcomeActivity.this, StarrySkyActivity.class);
//			startActivity(intent);
////			this.finish();
//		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);
		
		Button sensorButton = (Button) findViewById(R.id.sensorModeButton);
		Button touchButton = (Button) findViewById(R.id.touchModeButton);
		Button settingButton = (Button) findViewById(R.id.settingButton);
		Button helpButton = (Button) findViewById(R.id.helpButton);
		Button aboutButton = (Button) findViewById(R.id.aboutButton);
//		CheckBox skipCheckBox = (CheckBox) findViewById(R.id.skipWelcomePageCheckBox);
		
		sensorButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Editor edit = pref.edit();
				edit.putInt(
						PreferencesMap.getKey(PreferencesMap.OPERATION_MODE), 
						StarrySkyActivity.SENSOR);
				edit.commit();
				
				Intent intent = new Intent(
						WelcomeActivity.this, StarrySkyActivity.class);
				startActivity(intent);
			}
		});
		
		touchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Editor edit = pref.edit();
				edit.putInt(
						PreferencesMap.getKey(PreferencesMap.OPERATION_MODE), 
						StarrySkyActivity.TOUCH);
				edit.commit();
				
				Intent intent = new Intent(
						WelcomeActivity.this, StarrySkyActivity.class);
				startActivity(intent);
			}
		});
		
		settingButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						WelcomeActivity.this, SettingActivity.class);
				startActivity(intent);
			}
		});
		
		helpButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						WelcomeActivity.this, HelpActivity.class);
				startActivity(intent);
			}
		});
		
		aboutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						WelcomeActivity.this, AboutActivity.class);
				startActivity(intent);
			}
		});

//		skipCheckBox.setChecked(pref.getBoolean(
//				PreferencesMap.getKey( PreferencesMap.SKIP_WELCOME_PAGE ), 
//				(Boolean) PreferencesMap.getDefaultValue( PreferencesMap.SKIP_WELCOME_PAGE )));
//		skipCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				Editor editor = pref.edit();
//				editor.putBoolean(
//						PreferencesMap.getKey(PreferencesMap.SKIP_WELCOME_PAGE), isChecked);
//				editor.commit();
//			}
//		});
	}

}
