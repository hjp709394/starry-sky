package com.starrysky.math;

// TODO: Add startIndex and count for most of the method to limit the operation
// on the sub-range of the total array
public class Math3D {
	
	/* Should not be initialized */
	private Math3D() {}
	
	static public float[] crossProduct(float[] vU, float[] vV, float[] vResult) {
		vResult[0] = vU[1]*vV[2] - vV[1]*vU[2];
		vResult[1] = -vU[0]*vV[2] + vV[0]*vU[2];
		vResult[2] = vU[0]*vV[1] - vV[0]*vU[1];
		//normalize(vResult, vResult);
		return vResult;		// for calling chain's convenience
	}	

	static public float dotProduct(float[] vU, float[] vV) {
		return vU[0]*vV[0] + vU[1]*vV[1] + vU[2]*vV[2]; 
	}

	static public float[] vectorMinus(float[] vU, float[] vV, float[] vResult) {
		vResult[0] = vU[0] - vV[0];
		vResult[1] = vU[1] - vV[1];
		vResult[2] = vU[2] - vV[2];
		return vResult;
	}

	static public float[] vectorAdd(float[] vU, float[] vV, float[] vResult) {
		vResult[0] = vU[0] + vV[0];
		vResult[1] = vU[1] + vV[1];
		vResult[2] = vU[2] + vV[2];
		return vResult;
	}
	
	static public float[] scaleVector(float scalar, float[] vU, float[] vResult) {
		vResult[0] = vU[0] * scalar;
		vResult[1] = vU[1] * scalar;
		vResult[2] = vU[2] * scalar;
		return vResult;
	}
	
	/* Rotate vector vU, so the last column of mRotation, 
	 * which is one transformation vector, is not used.
	 * mRotation should be float[4 * 4] and in column first order.
	 * vU should be float[3] or float[4] with vU[4] = 1.
	 **/
	static public float[] rotateVector(float[] vU, float[] mRotation, float[] vResult) {
		vResult[0] = mRotation[0] * vU[0] + mRotation[4] * vU[1] + mRotation[8] *  vU[2];
		vResult[1] = mRotation[1] * vU[0] + mRotation[5] * vU[1] + mRotation[9] *  vU[2];
		vResult[2] = mRotation[2] * vU[0] + mRotation[6] * vU[1] + mRotation[10] * vU[2];
		return vResult;
	}
	
//	static public float[] multiplyMatrix(float[] m1, float[] m2, float[] result) {
//		return result;
//	}

	/* loadIndentityMatrix make mMatrix a identity matrix,
	 * mMatrix should be the size of 4*4 
	 **/
	static public float[] loadIdentityMatrix(float[] mMatrix) {
		for (int i = 0; i < 16; ++i) {
			mMatrix[i] = 0;
		} 
		for (int i = 0; i < 4; ++i) {
			mMatrix[i * 5] = 1.0f;
		}
		return mMatrix;
	}

	/* normalize vector vU, vResult can be the same as vU */
	static public float[] normalize(float[] vU, float[] vResult) {
		float length = (float) Math.sqrt(vU[0] * vU[0] + vU[1] * vU[1] + vU[2] * vU[2]);
		vResult[0] = vU[0] / length;
		vResult[1] = vU[1] / length;
		vResult[2] = vU[2] / length;
		return vResult;
	}

	/* get rotation matrix from vU, rotate angel surrounding vector vU */
	static public float[] getRotationMatrix(float angle, float[] vU, float[] mRotation) {
		float sinSave, cosSave, oneMinusCos;
		float xx, yy, zz, xy, yz, zx, xs, ys, zs;

		// If NULL vector passed in, this will blow up...
		if(vU[0] == 0.0f && vU[1] == 0.0f && vU[2] == 0.0f) {
			loadIdentityMatrix(mRotation);
			return mRotation;
		}

		normalize(vU, vU);

		sinSave = (float)Math.sin(angle);
		cosSave = (float)Math.cos(angle);
		oneMinusCos = 1.0f - cosSave;

		xx = vU[0] * vU[0];
		yy = vU[1] * vU[1];
		zz = vU[2] * vU[2];
		xy = vU[0] * vU[1];
		yz = vU[1] * vU[2];
		zx = vU[2] * vU[0];
		xs = vU[0] * sinSave;
		ys = vU[1] * sinSave;
		zs = vU[2] * sinSave;

		mRotation[0] = (oneMinusCos * xx) + cosSave;
		mRotation[4] = (oneMinusCos * xy) - zs;
		mRotation[8] = (oneMinusCos * zx) + ys;
		mRotation[12] = 0.0f;

		mRotation[1] = (oneMinusCos * xy) + zs;
		mRotation[5] = (oneMinusCos * yy) + cosSave;
		mRotation[9] = (oneMinusCos * yz) - xs;
		mRotation[13] = 0.0f;

		mRotation[2] = (oneMinusCos * zx) - ys;
		mRotation[6] = (oneMinusCos * yz) + xs;
		mRotation[10] = (oneMinusCos * zz) + cosSave;
		mRotation[14] = 0.0f;

		mRotation[3] = 0.0f;
		mRotation[7] = 0.0f;
		mRotation[11] = 0.0f;
		mRotation[15] = 1.0f;

		return mRotation;
	}

	/** Get one vector vResult such that vF, vU, vResult are in the same plane,
	 *  while vResult is vertical to vF and 
	 *  (vF X vU) and (vResult X vU) points to the same direction, 
	 *  in which X refers to cross product. 
	 **/
	public static float[] getVerticalVector(float[] vF, float[] vU, float[] vResult) {
		float upDotForward;
		upDotForward = Math3D.dotProduct(vF, vU);
		Math3D.scaleVector(upDotForward, vF, vResult);
		Math3D.vectorMinus(vU, vResult, vResult);
		Math3D.normalize(vResult, vResult);
		
		return vResult;
	}
	
	public static float[] vectorAssign(float[] vSrc, float[] vDst) {
		System.arraycopy(vSrc, 0, vDst, 0, 3);
		return vDst;
	}

	/**
	 * Get matrix used for axis transformation.
	 * @param vUp
	 * vUp refers to the y axis.
	 * @param vForward
	 * vForward refers to the z axis
	 * @param mMatrix
	 * mMatrix transform the Coordinates (a,b,c) in coordinate system XYZ to 
	 * (a',b',c') in coordinate system X'Y'Z'. 
	 * @return
	 * return mMatrix for calling chain's convenience
	 */
	public static float[] getMatrixFromAxis(float[] vUp, float[] vForward, float[] vLocation, float[] mMatrix) {
	    float[] vXAxis = new float[3];       // Derived X Axis
	    
	    // Calculate X Axis
	    Math3D.crossProduct(vUp, vForward, vXAxis);
	    
	    // Just populate the matrix
	    // X column vector
	    System.arraycopy(vXAxis, 0, mMatrix, 0, 3);
	    //memcpy(mMatrix, vXAxis, sizeof(GLTVector3));
	    mMatrix[3] = 0.0f;
	    
	    // y column vector
	    System.arraycopy(vUp, 0, mMatrix, 4, 3);
	    //memcpy(mMatrix+4, pFrame->vUp, sizeof(GLTVector3));
	    mMatrix[7] = 0.0f;
	    
	    // z column vector
	    System.arraycopy(vForward, 0, mMatrix, 8, 3);
	    //memcpy(mMatrix+8, pFrame->vForward, sizeof(GLTVector3));
	    mMatrix[11] = 0.0f;
	    
	    // Translation/Location vector
	    System.arraycopy(vLocation, 0, mMatrix, 12, 3);
	    //memcpy(mMatrix+12, pFrame->vLocation, sizeof(GLTVector3));
	    mMatrix[15] = 1.0f;
	    
	    return mMatrix;
	}
}
