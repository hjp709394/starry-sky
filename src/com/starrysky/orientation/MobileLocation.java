package com.starrysky.orientation;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;
import android.content.Context;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

/**
 * Require permission: ACCESS_COARSE_LOCATION, READ_PHONE_STATE, INTERNET.
 */
public class MobileLocation {
	
	private static float[] longitudeAndLatitude;
	
	public static boolean getLatestLocation(Context context, float[] position) {
		// Position for Chengdu
//		longitudeAndLatitude[0] = 104.06f;
//		longitudeAndLatitude[1] = 30.67f;
		
		// Location not change very much. Use values retrieved last time.
		if (MobileLocation.longitudeAndLatitude != null) {
			System.arraycopy(MobileLocation.longitudeAndLatitude, 0, position, 0, 3);
			return true;
		}
		
		MobileLocation.longitudeAndLatitude = new float[] { 0.0f, 0.0f, 0.0f };
		boolean isSuccessful = getLocation(context, MobileLocation.longitudeAndLatitude);
		System.arraycopy(MobileLocation.longitudeAndLatitude, 0, position, 0, 3);
		return isSuccessful;
	}
	
	public static boolean getLocation(Context context, float[] longitudeAndLatitude) {
		List<CellIDInfo> list = null;
		try {
			list = getCellIDInfo(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (list == null || list.size() == 0) {
			return false;
		}
		
		Location location = callGear(list);
		if (location == null) {
			return false;
		}
		
		longitudeAndLatitude[0] = (float) location.getLongitude();
		longitudeAndLatitude[1] = (float) location.getLatitude();
		longitudeAndLatitude[2] = (float) location.getAccuracy();
		return true;
	}
	
	private static class CellIDInfo {
		public int cellId;			// ID of base station.
		public String mobileCountryCode;
		public String mobileNetworkCode;
		public int locationAreaCode;
		public String radioType;
	}
	
	public static ArrayList<CellIDInfo> getCellIDInfo(Context context) 
			throws Exception{
		
		TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		
		ArrayList<CellIDInfo> CellID = new ArrayList<CellIDInfo>();
		CellIDInfo currentCell = new CellIDInfo();

		int type = manager.getNetworkType();
//		int phoneType = manager.getPhoneType();
		
		if (type == TelephonyManager.NETWORK_TYPE_GPRS				// GSM
				|| type == TelephonyManager.NETWORK_TYPE_EDGE
				|| type == TelephonyManager.NETWORK_TYPE_HSDPA) {
			GsmCellLocation gsm = ((GsmCellLocation) manager.getCellLocation());
			if (gsm == null) {
				return null;
			}
				
			int lac = gsm.getLac();
			String mcc = manager.getNetworkOperator().substring(0, 3);
			String mnc = manager.getNetworkOperator().substring(3, 5);
			
			currentCell.cellId = gsm.getCid();
			currentCell.mobileCountryCode = mcc;
			currentCell.mobileNetworkCode = mnc;
			currentCell.locationAreaCode = lac;
			
			currentCell.radioType = "gsm";
			
			CellID.add(currentCell);
			
			// Obtaining base station information in the neighborhood
			List<NeighboringCellInfo> list = manager.getNeighboringCellInfo();
			int size = list.size();
			for (int i = 0; i < size; i++) {
				CellIDInfo info = new CellIDInfo();
				info.cellId = list.get(i).getCid();
				info.mobileCountryCode = mcc;
				info.mobileNetworkCode = mnc;
				info.locationAreaCode = lac;
			
				CellID.add(info);
			}
		} else if (type == TelephonyManager.NETWORK_TYPE_CDMA		// CDMA of telecommunication
				|| type == TelephonyManager.NETWORK_TYPE_1xRTT
				|| type == TelephonyManager.NETWORK_TYPE_EVDO_0
				|| type == TelephonyManager.NETWORK_TYPE_EVDO_A) {
			
			CdmaCellLocation cdma = (CdmaCellLocation) manager.getCellLocation();	
			if (cdma == null) {
				return null;
			}
			
			int lac = cdma.getNetworkId();
			String mcc = manager.getNetworkOperator().substring(0, 3);
			String mnc = String.valueOf(cdma.getSystemId());
			int cid = cdma.getBaseStationId();
			
			currentCell.cellId = cid;
			currentCell.mobileCountryCode = mcc;
			currentCell.mobileNetworkCode = mnc;
			currentCell.locationAreaCode = lac;
	
			currentCell.radioType = "cdma";
			
			CellID.add(currentCell);
			
			List<NeighboringCellInfo> list = manager.getNeighboringCellInfo();
			int size = list.size();
			for (int i = 0; i < size; i++) {
				CellIDInfo info = new CellIDInfo();
				info.cellId = list.get(i).getCid();
				info.mobileCountryCode = mcc;
				info.mobileNetworkCode = mnc;
				info.locationAreaCode = lac;
			
				CellID.add(info);
			}
		}
		return CellID;
	}
	
	public static Location callGear(List<CellIDInfo> cellID) {
		if (cellID == null || cellID.size() == 0) 
			return null;

		DefaultHttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://www.google.com/loc/json");
		JSONObject holder = new JSONObject();

		try {
			holder.put("version", "1.1.0");
			holder.put("host", "maps.google.com");
			holder.put("home_mobile_country_code", cellID.get(0).mobileCountryCode);
			holder.put("home_mobile_network_code", cellID.get(0).mobileNetworkCode);
			holder.put("radio_type", cellID.get(0).radioType);
			holder.put("request_address", true);
			if ("460".equals(cellID.get(0).mobileCountryCode)) 
				holder.put("address_language", "zh_CN");
			else
				holder.put("address_language", "en_US");

			JSONObject data,current_data;

			JSONArray array = new JSONArray();

			current_data = new JSONObject();
			current_data.put("cell_id", cellID.get(0).cellId);
			current_data.put("location_area_code", cellID.get(0).locationAreaCode);
			current_data.put("mobile_country_code", cellID.get(0).mobileCountryCode);
			current_data.put("mobile_network_code", cellID.get(0).mobileNetworkCode);
			current_data.put("age", 0);
			current_data.put("signal_strength", -60);
			current_data.put("timing_advance", 5555);
			array.put(current_data);

			if (cellID.size() > 2) {
				for (int i = 1; i < cellID.size(); i++) {
					data = new JSONObject();
					data.put("cell_id", cellID.get(i).cellId);
					data.put("location_area_code", cellID.get(i).locationAreaCode);
					data.put("mobile_country_code", cellID.get(i).mobileCountryCode);
					data.put("mobile_network_code", cellID.get(i).mobileNetworkCode);
					data.put("age", 0);
					array.put(data);
				}
			}

			holder.put("cell_towers", array);

			StringEntity se = new StringEntity(holder.toString());
			Log.e("Location send", holder.toString());
			post.setEntity(se);
			HttpResponse resp = client.execute(post);

			HttpEntity entity = resp.getEntity();

			BufferedReader br = new BufferedReader(
					new InputStreamReader(entity.getContent()));
			StringBuffer sb = new StringBuffer();
			String result = br.readLine();
			while (result != null) {
				Log.e("Locaiton reseive-->", result);
				sb.append(result);
				result = br.readLine();
			}

			data = new JSONObject(sb.toString());

			data = (JSONObject) data.get("location");

			Location loc = new Location(LocationManager.NETWORK_PROVIDER);
			loc.setLatitude((Double) data.get("latitude"));
			loc.setLongitude((Double) data.get("longitude"));
			loc.setAccuracy(Float.parseFloat(data.get("accuracy").toString()));
			loc.setTime( System.currentTimeMillis());//AppUtil.getUTCTime());
			return loc;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}


	public static String getAddress(Location itude) throws Exception {
		String resultString = "";

		// Use Get instead of Post
		String urlString = String.format("http://maps.google.cn/maps/geo?key=abcdefg&q=%s,%s", itude.getLatitude(), itude.getLongitude());
		Log.i("URL", urlString);

		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(urlString);
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			BufferedReader buffReader = new BufferedReader(new InputStreamReader(entity.getContent()));
			StringBuffer strBuff = new StringBuffer();
			String result = null;
			while ((result = buffReader.readLine()) != null) {
				strBuff.append(result);
			}
			resultString = strBuff.toString();

			Log.e("resultAdress--->", resultString);

			// Obtain physical address via interpreting JSON data
			if (resultString != null && resultString.length() > 0) {
				JSONObject jsonobject = new JSONObject(resultString);
				JSONArray jsonArray = new JSONArray(jsonobject.get("Placemark").toString());
				resultString = "";
				for (int i = 0; i < jsonArray.length(); i++) {
					resultString = jsonArray.getJSONObject(i).getString("address");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error occurs while obtaining physical address: " + e.getMessage());
		} finally {
			get.abort();
			client = null;
		}

		return resultString;
	}
}
