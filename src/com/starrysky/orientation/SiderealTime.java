package com.starrysky.orientation;

import android.text.format.Time;

public class SiderealTime {
	
	public static double getMeridianSiderealTime() {
		Time time = new Time(Time.TIMEZONE_UTC);
		time.setToNow();
		
		// The Julian Day
		double JD = (int)(365.25 * time.year) + (int)(time.year / 400) - (int)(time.year / 100)
				+ (int)(30.59 * (time.month < 2 ? 13 + time.month : time.month - 1)) + time.monthDay + 1721088.5
				+ time.hour / 24.0 + time.minute / 1440.0 + time.second / 86400.0;
		
		// The Truncated Julian Day
		double TJD = JD - 2440000.5;
		
		// The sidereal time
		double theta = 24 * ( 0.671262 + 1.0027379094 * TJD );
		
		// The normalized sidereal time in the range of [0, 24)
		theta -= (int)(theta / 24) * 24;
		if (theta < 0) {
			theta += 24;
		}
		
		// The sidereal time in hh:mm:ss format
//		double t = theta;
//		int siderealHour = (int)t;
//		t = (t - siderealHour) * 60;
//		int siderealMinute = (int)t;
//		t = (t - siderealMinute) * 60;
//		int siderealSecond = (int)(t);
		
		return theta;
	}

}
