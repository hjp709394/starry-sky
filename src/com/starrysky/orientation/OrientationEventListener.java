package com.starrysky.orientation;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

// TODO: Add a activity notifying the sensor problem
/**
 * Register this listener to the TYPE_ACCELEROMETER and TYPE_MAGNETIC_FIELD both,
 * so that it can detect the orientation change.
 * The sensor may sometimes not work right. You shake the phone for a while and
 * it will works fine.
 */
public class OrientationEventListener implements SensorEventListener {
	
	private OnOrientationChangedListener o;
	
	// Use immediate coordinate base vector values when the phone is moving fast,
	// while using average coordinate base vector values when the phone is moving slowly,
	// in which way the stars display looks stable.
	
	// Use average values: 30
//	private static final int historyCount = 30;
//	private int curPos = 0;
//	private boolean isHistoryFilled = false;
//	private float[][] gravity = new float[historyCount][3];
//	private float[][] geomagnetic = new float[historyCount][3];
//	
//	private long[] timeStamp = new long[] { -1, -1 };
//	
//	private float[] last = new float[3];
//	
//	// Use threshold
//	private double threshold = 0.04;
	
//	private float[] last = new float[9];
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Do nothing;
	}
	
	private float[][] last = new float[2][3];
	private float[][] current = new float[2][3];
//	private float[] alpha = new float[] { 0.7f, 0.001f };
//	private int[] exp = new int[] { 3, 4 };
	private float[] alpha = new float[] { 0.7f, 0.001f };
	private int[] exp = new int[] { 3, 4 };

	@Override
	public void onSensorChanged(SensorEvent e) {
//		Log.i("OrientationEventListener", "onSensorChanged");
		if (o == null) {	// If not register OrientationListener, there is no need to continue.
			return;
		}
		
		// use cucumber algo: average and threshold
//		Log.i("StarrySkyActivity", "sensor event: " + ( timeStamp[0] - timeStamp[1] ) );
		
		// Recognize different Sensor. 
		// To detect orientation change, both accelerometer and magnetic sensor are needed.
//		switch ( e.sensor.getType() ) {	
//		case Sensor.TYPE_ACCELEROMETER:
//			System.arraycopy(e.values, 0, gravity[curPos], 0, 3);
//			timeStamp[0] = e.timestamp;
//			break;
//		case Sensor.TYPE_MAGNETIC_FIELD:
//			System.arraycopy(e.values, 0, geomagnetic[curPos], 0, 3);
//			timeStamp[1] = e.timestamp;
//			break;
//		default:
//			throw new IllegalArgumentException(
//					"Only accelerometer and magnetic sensor's sensor events are valid");
//		}
//		
//		// The sensor values pair are valid only when they produced at the same time.
////		if (timeStamp[0] != timeStamp[1]) {
////			return;
////		}
//		if (e.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
//			return;
//		
//		// Store the base vectors of the device coordinate system expressed in the horizontal coordinate system.
//		// For instance:
//		// eastNorthVertical[0,1,2] express the base vector x of the device coordinate system 
//		// with (east, north, vertical). It is also the same thing for base vector y and z.
//		float[] eastNorthVertical = new float[9];
//		
//		CoordinateSystem.gravityMagneticToHorizontal(gravity[curPos], geomagnetic[curPos], eastNorthVertical);
//
//		double distance = 0;		// The base vectors' distance
//		for (int i = 0; i < 3; ++i) {
//			int i3 = i * 3;
//			for (int j = 0; j < 3; ++j) {
//				distance += (eastNorthVertical[i3 + j] - last[i3 + j]) * (eastNorthVertical[i3 + j] - last[i3 + j]); 
//			}
//		}
//		if (distance < threshold) {		// If distance is less than threshold, use average base vector.
//			curPos = (curPos + 1) % historyCount;
//			if (!isHistoryFilled && curPos == 0) {
//				isHistoryFilled = true;
//			}
//			
//			int n;		// The first n history items are valid.
//			if (isHistoryFilled) {
//				n = historyCount;
//			} else {
//				n = curPos;
//			}
//			
//			float[] g = new float[3];
//			float[] m = new float[3];
//			for (int i = 0; i < n; ++i) {		// calculating average
//				for (int j = 0; j < 3; ++j) {
//					g[j] += gravity[i][j];
//					m[j] += geomagnetic[i][j];
//				}
//			}
//			for (int j = 0; j < 3; ++j) {
//				g[j] /= n;
//				m[j] /= n;
//			}
//			CoordinateSystem.gravityMagneticToHorizontal(g, m, eastNorthVertical);
//		} else {
//			curPos = 0;
//			isHistoryFilled = false;
////			Log.i("OrientationEventListener", "over threshold: " + distance);
//		}
//		System.arraycopy(eastNorthVertical, 0, last, 0, 9);	// storing last base vector.
		
		// Use google sky map's smooth algorithm: huge improvement
		int index = e.sensor.getType() == Sensor.TYPE_ACCELEROMETER ? 0 : 1;
		for (int i = 0; i < 3; ++i) {
			last[index][i] = current[index][i];
			float diff = e.values[i] - last[index][i];
			float correction = diff * alpha[index];
			for (int j = 1; j < exp[index]; ++j) {
				correction *= Math.abs(diff);
			}
			
//			Log.i(this.getClass().getName(), (e.sensor.getType() == Sensor.TYPE_ACCELEROMETER ? "acc" : "mag") + "-diff: " + diff + ", corretion: " + correction + "," + (Math.abs(correction) - Math.abs(diff)));
			
			if (correction > Math.abs(diff) ||
					correction < -Math.abs(diff)) 
				correction = diff;
			current[index][i] = last[index][i] + correction;
			
		}
		
		float[] eastNorthVertical = new float[9];
		CoordinateSystem.gravityMagneticToHorizontal(current[0], current[1], eastNorthVertical);
		
		// Actually do what you want with the horizontal coordinate system known.
		o.onOrientationChanged(eastNorthVertical);
	}
	
	public static interface OnOrientationChangedListener {
		/**
		 * Do something when the device's orientation is changed.
		 * 
		 * @param eastNorthVertical the float array storing 
		 * base vector of the device coordinate system 
		 * using the base vector of the horizontal coordinate system
		 * (east, north, vertical).
		 * base vector x in device coordinate system can be expressed with 
		 * (eastNorthVertical[0], eastNorthVertical[1], eastNorthVertical[2])
		 * in the horizontal coordinate system. 
		 * And it is similar with y and z in the device coordinate system.
		 */
		public void onOrientationChanged(float[] eastNorthVertical);
	}
	
	public void setOnOrientationChangedListener(OnOrientationChangedListener o) {
		this.o = o;
	}
}