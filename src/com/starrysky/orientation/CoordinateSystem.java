package com.starrysky.orientation;

import com.starrysky.math.Math3D;

import android.hardware.SensorManager;
import android.opengl.Matrix;

public class CoordinateSystem {
	
	public static float[] horizontalToEarth(float[] eastNorthVertical, float[] xyz, float longitude, float latitude) {		
		float cosLongitude = (float) Math.cos(longitude / 180.0 * Math.PI);
		float sinLongitude = (float) Math.sin(longitude / 180.0 * Math.PI);
		float cosLatitude = (float) Math.cos(latitude / 180.0 * Math.PI);
		float sinLatitude = (float) Math.sin(latitude / 180.0 * Math.PI);
		
		float[] v = new float[]{ cosLongitude * cosLatitude	, sinLatitude	, -sinLongitude * cosLatitude };
		float[] n = new float[]{ 
				-v[0] * sinLatitude / cosLatitude, 
				1 / cosLatitude -v[1] * sinLatitude / cosLatitude,
				-v[2] * sinLatitude / cosLatitude
		};
		float[] e = new float[] {
				n[1]*v[2] - v[1]*n[2],
				-n[0]*v[2] + v[0]*n[2],
				n[0]*v[1] - v[0]*n[1]
		};
		
		for (int i = 0; i < 3; ++i) {
			int i3 = i * 3;
			for (int j = 0; j < 3; ++j) {
				xyz[i3 + j] = eastNorthVertical[i3] * e[j] 
						+ eastNorthVertical[i3 + 1] * n[j] 
						+ eastNorthVertical[i3 + 2] * v[j];
			}
		}
		
		return xyz;
	}

	/** 
	 * Calculate the coordinates system.
	 * So that the base vector x, y, z in the device coordinate system
	 * is expressed by the horizontal coordinate system (east, north, vertical)
	 * @param g
	 * @param m
	 * @param eastNorthVertical
	 * @return
	 */
	public static float[] gravityMagneticToHorizontal(float[] g, float[] m, float[] eastNorthVertical) {
		float[] R = new float[16];
		float[] I = new float[16];
		float[] result = new float[16];
		SensorManager.getRotationMatrix(R, I, g, m);

		float[] z = {0, 0, 1, 1};		// Mobile ?
		float[] y = {0, 1, 0, 1};
		float[] x = {1, 0, 0, 1};
//		float[] z = {0, 0, 1, 1};		// Pad?
//		float[] y = {1, 0, 0, 1};
//		float[] x = {0, 1, 0, 1};
		
		Matrix.transposeM(result, 0, R, 0);
		System.arraycopy(result, 0, R, 0, 16);

		Matrix.multiplyMV(result, 0, R, 0, x, 0);
		System.arraycopy(result, 0, eastNorthVertical, 0, 3);

		Matrix.multiplyMV(result, 0, R, 0, y, 0);
		System.arraycopy(result, 0, eastNorthVertical, 3, 3);

		Matrix.multiplyMV(result, 0, R, 0, z, 0);
		System.arraycopy(result, 0, eastNorthVertical, 6, 3);
		
		return eastNorthVertical;
	}
	
	// TODO: Add argument start index and length.
	// TODO: support that xyz = zyzRotated so that it saves more memory. 
	public static void rotateByLongitudeAndLatitude(
				float longitude, 
				float latitude,
				float[] xyz,
				float[] xyzRotated) {
		float[] vY = new float[]{0, 1, 0};
		float[] vZ = new float[]{0, 0, 1};
		float[] mRotationY = new float[16];		// rotate around vector up(or y)
		float[] mRotationZ = new float[16];		// rotate around vector right(or x)
		float[] vResult1 = new float[3];
		float[] vResult2 = new float[3];
		int pointCount = xyz.length / 3; 
		
		Math3D.getRotationMatrix( (float)(longitude / 180 * Math.PI), 
				vY, mRotationY );
		Math3D.rotateVector(vZ, mRotationY, vResult1);
		Math3D.normalize(vResult1, vZ);
		Math3D.getRotationMatrix( (float)(latitude / 180 * Math.PI), 
				vZ, mRotationZ);
		
		// Here, to rotate the vector twice is faster than 
		// rotate it once using the combination rotation 
		// matrix M = mRotationRight X mRotationUp
		float[] vertex = new float[3];
		for (int i = 0; i < pointCount; i++) {
			System.arraycopy(xyz, i * 3, vertex, 0, 3);
			Math3D.rotateVector(vertex, mRotationY, vResult1);		// rotate around vector up
			Math3D.rotateVector(vResult1, mRotationZ, vResult2);	// rotate around vector right
			System.arraycopy(vResult2, 0, xyzRotated, i * 3, 3);
		}
	}
}
